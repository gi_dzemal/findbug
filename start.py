from file_processor import find_bugs
import configparser

# reading chunk_size setting from ini file
config = configparser.ConfigParser()
config.read('.ini')
if config.has_option('FILE', 'chunk_size'):
    chunk_size = config['FILE']['chunk_size']
else:
    chunk_size = 1024

# file_bug = "bug.txt"
# provide file name used as bug pattern
while True:
    file_bug = raw_input("Type the filename with bug pattern: ")
    # file_bug = "bug.txt"
    try:
        with open(file_bug) as f:
            bug = ''.join(f.read().split())
            if len(bug) > int(chunk_size):
                chunk_size = 1024
                print("Chunk size must be greater than bug length. \n Setting chunk size to default value!")
            break
    except IOError:
        print ("There is no such a file with BUG pattern, please enter valid file name")

# file_content = "landscape.txt"
# provide file name with content
while True:
    file_content = raw_input("Type the filename with CONTENT: ")
    # file_content = "landscape.txt"
    count = 0
    try:
        with open(file_content) as f:
            count = find_bugs(f, bug, int(chunk_size))
        break
    except IOError:
        print ("There is no such a CONTENT file, please enter valid file name")

print("\nYour search value of ' %s ' appears %s times in this file %s" % (bug, count, file_content))

