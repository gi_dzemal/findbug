def bugs_in_chunk(chunk, bug):
    file_to_string = ''.join(chunk.split())
    return file_to_string.count(bug)


def find_bugs(f, bug, chunk_size):
    count = 0
    prev_chunk = ''
    for chunk in iter(lambda: f.read(chunk_size), b''):
        chunk = ''.join(chunk.split())
        # append last 7 characters (bug length -1) as substring of previous chunk, because bug can be split in 2 chunks
        # half characters at the end of one chunk and the rest at the beginning of next chunk
        if len(prev_chunk) >= len(bug):
            chunk = prev_chunk[len(prev_chunk) - len(bug) - 1:len(prev_chunk)] + chunk
        else:
            chunk = prev_chunk + chunk
        prev_chunk = ''.join(chunk.split())
        count += bugs_in_chunk(chunk, bug)
    return count
